package com.afkl.exercises.spring;

import java.io.IOException;
import java.util.HashMap;

import javax.faces.webapp.FacesServlet;

import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletListenerRegistrationBean;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.codahale.metrics.MetricRegistry;
import com.sun.faces.config.ConfigureListener;

@SpringBootApplication
public class Bootstrap {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(Bootstrap.class, args);
        
    }
    @Bean
 	public static ViewScope viewScope() {
 		return new ViewScope();
 	}

 	@Bean
 	public static CustomScopeConfigurer scopeConfigurer() {
 		CustomScopeConfigurer configurer = new CustomScopeConfigurer();
 		HashMap<String, Object> hashMap = new HashMap<String, Object>();
 		hashMap.put("view", viewScope());
 		configurer.setScopes(hashMap);
 		return configurer;
 	}

 	@Bean
 	public FacesServlet facesServlet() {
 		return new FacesServlet();
 	}

 	@Bean
 	public ServletRegistrationBean facesServletRegistration() {
 		ServletRegistrationBean registration = new ServletRegistrationBean(
 				facesServlet(), "*.xhtml");
 		registration.setName("FacesServlet");
 		return registration;
 	}

 	@Bean
 	public ServletListenerRegistrationBean<ConfigureListener> jsfConfigureListener() {
 		return new ServletListenerRegistrationBean<ConfigureListener>(
 				new ConfigureListener());
 	}
 	@Bean
 	public RequestCountMonitoringConfiguration requestCountMonitoringConfiguration(MetricRegistry metricRegistry){
 		return new RequestCountMonitoringConfiguration (metricRegistry);
 	}
  
}
